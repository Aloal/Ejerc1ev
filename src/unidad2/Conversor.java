package unidad2;

import java.io.BufferedReader;  //import java.io.*, importamos las 3 clases
import java.io.IOException;
import java.io.InputStreamReader;

public class Conversor {

public static void main(String[] args) throws IOException {
		
		BufferedReader br = new BufferedReader (new InputStreamReader (System.in));
		
		double dolar;
		double euro;
		
		System.out.println("Introduzca valor  en euros");  //Pedimos por teclado los euros
		String cantidad=br.readLine();                      // Almacenamos la entrada e invocamos el metodo para leer teclado
		
		euro= Double.parseDouble(cantidad);     //convertimos el string cantidad en double
		
		dolar=euro/0.85;
		
		
		System.out.printf( euro + "� equivalen a estos dolares: %.2f", dolar);
		
		
		
		
		
		

	}

}
