package unidad2;

import java.io.BufferedReader;  //import java.io.*, importamos las 3 clases
import java.io.IOException;
import java.io.InputStreamReader;

public class Calificaciones {

	public static void main(String[] args) throws IOException {
		
				
		float examenM;
		float tarea1M;
		float tarea2M;
		float tarea3M;
		float examenF;
		float tarea1F;
		float tarea2F;
		float examenQ;
		float tarea1Q;
		float tarea2Q;
		float tarea3Q;
		double promedioM;
		double promedioF;
		double promedioQ;
		double promedio;
		
		BufferedReader in = new BufferedReader (new InputStreamReader (System.in));
		
		//Pedimos las notas de matem�ticas

		System.out.println("Nota del examen de matem�ticas:");
		examenM=Float.parseFloat(in.readLine());
		
		System.out.println("Nota tarea 1 de matem�ticas:");
		tarea1M=Float.parseFloat(in.readLine());
		
		System.out.println("Nota tarea 2 de matem�ticas:");
		tarea2M=Float.parseFloat(in.readLine());
		
		System.out.println("Nota tarea 3 de matem�ticas:");
		tarea3M=Float.parseFloat(in.readLine());
		
		//Ahora hacemos lo mismo con Fisica
	
		 
		System.out.println("Nota del examen de f�sica:");
		examenF=Float.parseFloat(in.readLine());
		
		System.out.println("Nota tarea 1 de fisica:");
		tarea1F=Float.parseFloat(in.readLine());
		
		System.out.println("Nota tarea 2 de f�sica:");
		tarea2F=Float.parseFloat(in.readLine());
		
		//Lo mismo con Quimica
		
		
		System.out.println("Nota del examen de qu�mica:");
		examenQ=Float.parseFloat(in.readLine());
		
		System.out.println("Nota tarea 1 de qu�mica:");
		tarea1Q=Float.parseFloat(in.readLine());
		
		System.out.println("Nota tarea 2 de qu�mica:");
		tarea2Q=Float.parseFloat(in.readLine());
		
		System.out.println("Nota tarea 3 de qu�mica:");
		tarea3Q=Float.parseFloat(in.readLine());
		
		
	  
		promedioM= (examenM*0.9)+((tarea1M+tarea2M+tarea3M)/3)*0.10;
		
		promedioF= (examenF*0.9)+((tarea1F+tarea2F)/2)*0.20;
		
		promedioQ= (examenM*0.9)+((tarea1M+tarea2M+tarea3M)/3)*0.15;
		
		promedio= (promedioM+promedioF+promedioQ)/3;
		
		
		System.out.printf("La nota de matem�ticas es: %.2f ", promedioM);
		System.out.printf("La nota de fisica es: %.2f ", promedioF);
		System.out.printf("La nota de quimica es: %.2f ", promedioQ);
				
		System.out.printf("La nota total es: %.2f ", promedio);
		
		
		
	}
	
}
		
		